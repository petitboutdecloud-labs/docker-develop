DOCKER_HOST = 10.0.21.10:2376

.PHONY: all
all: up

.PHONY: up
up: consul.up proxy.up apps.up

.PHONY: ps
ps: consul.ps proxy.ps apps.ps

.PHONY: down
down: proxy.down apps.down consul.down

.PHONY: consul.up
consul.up:
	@echo ' *** Consul up:'
	@docker-compose -f consul/docker-compose.yml up -d

.PHONY: consul.ps
consul.ps:
	@echo ' *** Consul status:'
	@docker-compose -f consul/docker-compose.yml ps

.PHONY: consul.down
consul.down:
	@echo ' *** Consul down:'
	@docker-compose -f consul/docker-compose.yml down

.PHONY: proxy.up
proxy.up:
	@echo ' *** Proxy up:'
	@docker-compose -f proxy/docker-compose.yml up -d

.PHONY: proxy.ps
proxy.ps:
	@echo ' *** Proxy status:'
	@docker-compose -f proxy/docker-compose.yml ps

.PHONY: proxy.down
proxy.down:
	@echo ' *** Proxy down:'
	@docker-compose -f proxy/docker-compose.yml down

.PHONY: apps.up
apps.up:
	@echo ' *** Apps up:'
	@docker-compose -f apps/docker-compose.yml up -d

.PHONY: apps.ps
apps.ps:
	@echo ' *** Apps status:'
	@docker-compose -f apps/docker-compose.yml ps

.PHONY: apps.down
apps.down:
	@echo ' *** Apps down:'
	@docker-compose -f apps/docker-compose.yml down
