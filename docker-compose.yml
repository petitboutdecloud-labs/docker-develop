version: '3.7'

services:

  rundeck:
    image: ${RUNDECK_IMAGE:-rundeck/rundeck:SNAPSHOT}
    restart: always
    environment:
      RUNDECK_GRAILS_URL: http://rundeck.${DOMAIN}
      RUNDECK_SERVER_FORWARDED: 'true'
      SERVICE_NAME: rundeck-1
      SERVICE_TAGS: 'urlprefix-rundeck.${DOMAIN}/'
      SERVICE_CHECK_HTTP: '/'
      SERVICE_CHECK_INTERVAL: '5s'
    hostname: rundeck
    tty: true
    volumes:
      - rundeck-1:/home/rundeck/server/data
    expose:
      - 4440

  ansible:
    image: ansible:latest
    build: ./ansible
    restart: always
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock
      - ./ansible:/ansible
    ports:
      - '127.0.0.1:22:22'

  nexus-1:
    image: sonatype/nexus3
    restart: always
    environment:
      SERVICE_NAME: nexus-1
      SERVICE_TAGS: 'urlprefix-nexus.${DOMAIN}/'
      SERVICE_CHECK_HTTP: '/'
      SERVICE_CHECK_INTERVAL: '5s'
    hostname: repository
    expose:
     - "8081"
    volumes:
     - '/srv/my-dev-space/repository-1/data:/nexus-data'
    ulimits:
      nproc: 65535
      nofile:
        soft: 32000
        hard: 40000

  gitlab-1:
    image: 'gitlab/gitlab-ce:latest'
    restart: always
    environment:
      SERVICE_22_NAME: gitlab-1-ssh
      SERVICE_22_TAGS: urlprefix-:2222 proto=tcp
      SERVICE_22_CHECK_TCP: 'true'
      SERVICE_22_CHECK_INTERVAL: 5s
      SERVICE_22_CHECK_TIMEOUT: 3s
      SERVICE_80_NAME: gitlab-1
      SERVICE_80_TAGS: 'urlprefix-gitlab.${DOMAIN}/'
      SERVICE_80_CHECK_HTTP: '/project/help'
      SERVICE_443_NAME: gitlab-1-https
      # SERVICE_443_TAGS: 'urlprefix-gitlab.${DOMAIN}/'
      # SERVICE_443_CHECK_TCP: 'true'
      # SERVICE_443_CHECK_INTERVAL: '30s'
      # SERVICE_443_CHECK_TIMEOUT: '10s'
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab.${DOMAIN}/'
      GITLAB_SSH_PORT: '22'
      GITLAB_PORT: '80'
      GITLAB_HTTPS: 'false'
      SSL_SELF_SIGNED: 'false'
    hostname: gitlab
    expose:
      - 80
      - 22
    volumes:
      - '/srv/my-dev-space/project/config:/etc/gitlab'
      - '/srv/my-dev-space/project/logs:/var/log/gitlab'
      - '/srv/my-dev-space/project/data:/var/opt/gitlab'
    ulimits:
      nproc: 65535
      nofile:
        soft: 32000
        hard: 40000

  mariadb-1:
    image: mariadb:10.2
    environment:
      MYSQL_USER: 'codimd'
      MYSQL_PASSWORD: 'codimdpass'
      MYSQL_DATABASE: 'codimd'
      MYSQL_ALLOW_EMPTY_PASSWORD: 'true'
      SERVICE_NAME: mariadb-1
    volumes:
      - mariadb-1:/var/lib/mysql
      - ./codimd/resources/utf8.cnf:/etc/mysql/conf.d/utf8.cnf
    restart: always

  codimd-1:
    build:
     context: ./codimd/
     dockerfile: ./debian/Dockerfile
     args:
       - "VERSION=master"
       - "CODIMD_REPOSITORY=https://github.com/codimd/server.git"
    image: codimd-server
    environment:
      CMD_DB_URL: 'mysql://codimd:codimdpass@mariadb-1:3306/codimd'
      CMD_DOMAIN: 'codimd.${DOMAIN}'
      SERVICE_NAME: codimd-1
      SERVICE_TAGS: 'urlprefix-codimd.${DOMAIN}/'
      SERVICE_CHECK_HTTP: '/index.html'
      SERVICE_CHECK_INTERVAL: 5s
    expose:
      - 3000
    volumes:
      - ./codimd/resources/config.json:/codimd/config.json
      - ./codimd/resources/index.html:/codimd/public/index.html
    restart: always
    depends_on:
      - mariadb-1

  registrator-1: &consul-registrator
    image: gliderlabs/registrator:latest
    restart: always
    network_mode: host
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock
    command: |
      -cleanup
      -internal
      consul://127.0.0.1:8500
  
  fabio-1:
    image: fabiolb/fabio
    environment:
      SERVICE_9998_NAME: fabio
      SERVICE_9998_TAGS: urlprefix-fabio.${DOMAIN}/
      SERVICE_9998_CHECK_HTTP: '/routes'
      SERVICE_9998_CHECK_INTERVAL: '5s'
    network_mode: host
    ports:
      - '80:80'
      - '443:443'
      - '2222:2222'
      - '9998:9998'
    command: |
      -proxy.addr ':80;proto=http,:443;proto=tcp,:2222;proto=tcp'
      -registry.consul.addr '127.0.0.1:8500'

  consul-1:
    image: consul:latest
    restart: always
    network_mode: host
    environment:
      SERVICE_8500_NAME: consul-ui
      SERVICE_8500_TAGS: 'urlprefix-consul.${DOMAIN}/'
      SERVICE_8500_CHECK_HTTP: '/ui'
      SERVICE_8500_CHECK_INTERVAL: 5s
      SERVICE_8500_CHECK_TIMEOUT: 10s
    ports:
      - '8400:8400'
      - '8500:8500'
      - '8600:8600'
      - '8600:8600/udp'
    command: 'agent -ui -server -bootstrap-expect 1 -advertise 127.0.0.1 -client 0.0.0.0'

volumes:
  mariadb-1:
  rundeck-1: